from werkzeug import generate_password_hash, check_password_hash
from personal import db
from sqlalchemy.orm import backref


class Admin(db.Model):
    __tablename__ = 'Admin'
    id = db.Column(db.INT, primary_key=True)
    username = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(120))

    pagename = db.Column(db.String(100))
    title = db.Column(db.String(300))
    above = db.Column(db.String(1000))
    below = db.Column(db.String(1000))
    logo = db.Column(db.String(200))
    favicon = db.Column(db.String(200))
    tagline = db.Column(db.String(300))

    theme = db.Column(db.String(100))

    def __init__(self, 
                username, 
                password, 
                pagename=None, 
                title=None, 
                above=None, 
                below=None, 
                logo=None, 
                favicon=None, 
                tagline=None, 
                theme=None):
            
        self.username = username.title()
        self.set_password(password)

        self.pagename = pagename 
        self.title = title 
        self.above = above 
        self.below = below 
        self.logo = logo 
        self.favicon = favicon 
        self.tagline = tagline 

        self.theme = theme


    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)


class Sections(db.Model):
    __tablename__ = 'Sections'

    id = db.Column(db.INT, primary_key=True)
    levelname = db.Column(db.String(20), unique=True, nullable=False)
    paneltype = db.Column(db.String(10), nullable=False)
    order = db.Column(db.INT, nullable=False)

    def __init__(self, 
                levelname=None,
                paneltype=None,
                order=None
                ):
        self.levelname = levelname
        self.paneltype = paneltype
        self.order = order


class Subsections(db.Model):
    __tablename__ = 'Subsections'

    id = db.Column(db.INT, primary_key=True)
    section_id = db.Column(db.INT, db.ForeignKey('Sections.id', ondelete='CASCADE'), nullable=False)
    levelname = db.Column(db.String(40), unique=True, nullable=False)
    order = db.Column(db.INT, nullable=False)
    section = db.relationship('Sections', backref=backref('subsections', passive_deletes=True))

    def __init__(self, 
                section_id=None,
                levelname=None,
                order=None
                ):
        self.section_id = section_id
        self.levelname = levelname
        self.order = order


class Content(db.Model):
    __tablename__ = 'Content'

    id = db.Column(db.INT, primary_key=True)
    levelname = db.Column(db.String(400))
    order = db.Column(db.INT, nullable=False)

    subsection_id = db.Column(db.INT, db.ForeignKey('Subsections.id', ondelete='CASCADE'), nullable=False)
    picture_url = db.Column(db.String(400))
    external_url = db.Column(db.String(400))

    date = db.Column(db.DateTime)
    markdown = db.Column(db.String(10000), nullable=False)
    featured = db.Column(db.Boolean)
    subsection = db.relationship('Subsections', backref=backref('content', passive_deletes=True))

    def __init__(self, 
                levelname=None,
                order=None,
                subsection_id=None,
                picture_url=None,
                external_url=None,
                date=None,
                markdown=None,
                featured=False
                ):
        self.levelname = levelname
        self.order = order
        self.subsection_id = subsection_id
        self.picture_url = picture_url
        self.external_url = external_url
        
        self.date = date
        self.markdown = markdown
        self.featured = featured
