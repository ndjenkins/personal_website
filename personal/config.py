import os

class BaseConfig(object):
    """Base configuration."""

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    if os.environ.get('DATABASE_URL') is None:
        DEBUG = True
        SQLALCHEMY_DATABASE_URI = f'postgresql://postgres:{os.environ["POSTGRES_PASSWORD"]}@localhost:5432/{os.environ["POSTGRES_DB"]}'
    else:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
        DEBUG = False

    SECRET_KEY = os.environ.get('SECRET_KEY') if os.environ.get('SECRET_KEY') else "secret1"
    SECURITY_PASSWORD_SALT = os.environ.get('SECURITY_PASSWORD_SALT') if os.environ.get('SECURITY_PASSWORD_SALT') else "secret2"
    WTF_CSRF_ENABLED = True
