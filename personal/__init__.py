from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_pagedown import PageDown

app = Flask(__name__)
app.config.from_object('personal.config.BaseConfig')
pagedown = PageDown(app)
db = SQLAlchemy(app)

import personal.views