from flask_wtf import Form
from wtforms import StringField, PasswordField, SubmitField, BooleanField, RadioField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, InputRequired, NumberRange
from personal.models import db, Admin
from flask_pagedown.fields import PageDownField

class Login_Form(Form):
    username = StringField("Username", 
        validators=[DataRequired("Please your username"),
                    Length(min=5, max=25)])
    password = PasswordField("Password", 
        validators=[DataRequired("Please enter a password"),
                    Length(min=5, max=25, message="Passwords must be a minimum of 6 characters")])
    submit = SubmitField("Sign in")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        # self.user = None
        # self.pw = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = Admin.query.filter_by(username=self.username.data.title()).first()
        if not user:
            self.username.errors.append('Check A')
            return False

        if not user.check_password(self.password.data):
            self.username.errors.append('Check B')
            return False

        # self.user = user
        # self.pw = pw
        return True

class Sections_Add_Form(Form):
    paneltype = RadioField("Section", choices=[], validators=[InputRequired()])  
    levelname = StringField("Name", validators=[DataRequired()])
    order = IntegerField("Ordering", default=None, 
        validators=[NumberRange(message='Range should be between 1 to 1000', min=1, max=1000)])
    submit = SubmitField("Submit Section")

class Subsections_Add_Form(Form):
    section_id = RadioField("Section", choices=[], validators=[InputRequired()])  
    levelname = StringField("Name", validators=[DataRequired()])
    order = IntegerField("Ordering", default=None, 
        validators=[NumberRange(message='Range should be between 1 to 1000', min=1, max=1000)])
    submit = SubmitField("Submit subsection")


class Content_Add_Form(Form):
    subsection_id = RadioField("Subsection", choices=[], validators=[InputRequired()])  
    levelname = StringField("Name", validators=[DataRequired()])
    order = IntegerField("Ordering", default=None, 
        validators=[NumberRange(message='Range should be between 1 to 1000', min=1, max=1000)])
    picture_url = StringField("URL for optional image")
    external_url = StringField("URL for optional external link")
    markdown = PageDownField("Markdown content", validators=[DataRequired()])
    featured = BooleanField("Featured item?")
    submit = SubmitField("Submit content")


class Admin_Form(Form):
    pagename = StringField("Pagename", validators=[DataRequired()])
    title = PageDownField("Title", validators=[DataRequired()])
    above = PageDownField("Above the line", validators=[DataRequired()])
    below = PageDownField("Below the line", validators=[DataRequired()])
    logo = StringField("URL for logo", validators=[DataRequired()])
    favicon = StringField("URL for website icon", validators=[DataRequired()])
    tagline = PageDownField("Tagline", validators=[DataRequired()])

    theme = SelectField("Website theme", choices=[], validators=[DataRequired()])
    password = StringField("password", validators=[DataRequired()])
    submit = SubmitField("Submit content")