# coding: utf-8
from flask import Flask, flash, render_template, redirect, url_for, request, jsonify, session, make_response, abort, Markup
from flask_login import LoginManager, login_user, login_required, logout_user, current_user

import os
import json
import markdown
import pandas as pd
from datetime import datetime as dt

from personal import app
from personal.my_forms import *
from personal.models import *

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

@app.context_processor
def get_master_details():
    def name_to_url(name):
        return name.replace(" ","_").lower()
    def markupify(str):
        return markdown.markdown(str)
    return dict(master=Admin.query.get(1).__dict__, 
                section=Sections.query.order_by("order").all(), 
                name_to_url=name_to_url, 
                markupify=markupify, 
                len=len)

@app.route('/')
def home():
    featured = Content.query.order_by("order").filter_by(featured=True).all()
    return render_template('index.html', featured=featured)


@app.route('/<link>')
def content(link):
    content = [x for x in Sections.query.order_by("order").all() if x.levelname.upper()==link.upper()]
    if not content:
        flash("Page does not exist", "warning")
        return redirect(url_for('home'))
    return render_template('content.html', content=content[0])


@app.route('/<link>/<link2>')
def content_fine(link, link2):
    link = link.upper()
    link2 = link2.replace(" ","_").lower()

    content = [x for x in Content.query.all() \
        if x.subsection.section.levelname.upper()==link \
        and x.subsection.levelname.replace(" ","_").lower()==link2]
    if not content:
        flash("Could not find content", "warning")
        return redirect(url_for('home'))
    return render_template('content_close.html', content=content[0])


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.route('/manage', methods=['POST', 'GET'])
@login_required
def manage():
    sections = Sections.query.order_by("order").all()
    subsections = Subsections.query.order_by("order").all()
    contents = Content.query.order_by("order").all()
    return render_template('new_manage.html', sections=sections, 
        subsections=subsections, contents=contents)

@app.route('/admin', methods=['POST','GET'])
@login_required
def admin():
    admin = Admin.query.get(1)
    form = Admin_Form(request.form)

    themes = [(x,x) for x in os.listdir(os.path.join(app.static_folder, 'assets/css/themes'))]
    form.theme.choices = themes

    if request.method=="GET":
        form.pagename.data = admin.pagename
        form.title.data = admin.title
        form.above.data = admin.above
        form.below.data = admin.below
        form.logo.data = admin.logo
        form.favicon.data = admin.favicon
        form.tagline.data = admin.tagline
        form.theme.process_data(admin.theme)
        form.password.data = "*** only enter new data if you want to change your password ***"

    else:
        if form.validate_on_submit():
            admin.pagename = form.pagename.data
            admin.title = form.title.data
            admin.above = form.above.data
            admin.below = form.below.data
            admin.logo = form.logo.data
            admin.favicon = form.favicon.data
            admin.tagline = form.tagline.data       
            admin.theme = form.theme.data
            if form.password.data != "*** only enter new data if you want to change your password ***":
                admin.set_password(form.password.data)
            db.session.commit()
            
            flash("Admin details updated", "success")
            return redirect(url_for('home'))            
    return render_template('admin.html', admin=admin, form=form)

@login_manager.user_loader
def load_user(user_id):
    return Admin.query.get(user_id)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = Login_Form(request.form)
    if request.method == "POST":
        if form.validate():      
            user = Admin.query.filter_by(username=form.username.data.title()).first()
            login_user(user)
            flash('Logged in as ' + user.username.title() + '. Welcome!')
            return redirect(url_for('home'))
            
    return render_template('login.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Successfully logged out. See you soon!")
    return redirect(url_for('home'))


@app.route('/delete')
@login_required
def delete():
    _id = request.args.get("id")
    _type = request.args.get("type")
    confirm_delete = request.args.get("confirm")  

    if not _id or not _type:
        flash("Data does not exist", "warning")
        return redirect(url_for('manage'))

    _type = _type.title()
    table_list = ["Sections", "Subsections", "Content"]
    if _type not in table_list:
        print("Invalid page access")
        flash("Invalid table, does not exist", "warning")
        return redirect(url_for('manage'))

    # this line must follow the above sanity check
    record = eval(_type if _type in table_list else "force_eval_fail").query.get(_id)
    if not record:
        print("Invalid record access")
        flash("Invalid record, does not exist", "warning")
        return redirect(url_for('manage'))        

    if confirm_delete=="1":
    #TODO Works but needs a delete cascade
        db.session.delete(record)
        db.session.commit() 
        flash("Successfully deleted the " + _type, "success")
        return redirect(url_for('manage'))                
    else:
        return render_template('delete.html', _id=_id, _type=_type, record=record)




@app.route('/add', methods=['POST', 'GET'])
@login_required
def add():
    _type = request.args.get("type")

    if not _type:
        flash("Page does not exist", "warning")
        return redirect(url_for('manage'))

    _type = _type.title()
    table_list = ["Sections", "Subsections", "Content"]
    if _type not in table_list:
        print("Invalid page access")
        flash("Invalid table, does not exist", "warning")
        return redirect(url_for('manage'))

    form_type = _type + "_Add_Form" if _type in table_list else "force_eval_fail"

    form = eval(form_type)(request.form)

    paneltype_choices = [("1","Simple"), ("2","Subpanel"), ("3", "Article")]

    if _type=="Sections":
        form.paneltype.choices = paneltype_choices
        if request.method=="GET":
            #Added the [0] in case its an empty list / no website data
            form.order.data = max([x.order for x in Sections.query.all()] + [0]) + 1
        
    if _type=="Subsections":
        form.section_id.choices = [(str(x.id)+"", x.levelname) for x in Sections.query.order_by("order").all()]
        if request.method=="GET":
            form.order.data = max([x.order for x in Subsections.query.all()] + [0]) + 1

    if _type=="Content":
        form.subsection_id.choices = [(str(x.id)+"", x.section.levelname + " - " + x.levelname) for x in Subsections.query.order_by("order").all()]
        if request.method=="GET":
            form.order.data = max([x.order for x in Content.query.all()] + [0]) + 1


    if form.validate_on_submit():
        if _type=="Sections":
            new_data = Sections( 
                        levelname=form.levelname.data,
                        paneltype=paneltype_choices[int(form.paneltype.data)-1][1],
                        order=form.order.data)

        if _type=="Subsections":
            print(form.section_id.choices)
            new_data = Subsections( 
                        section_id=form.section_id.data,
                        levelname=form.levelname.data,
                        order=form.order.data)          

        if _type=="Content":
            new_data = Content(
                        levelname=form.levelname.data,
                        order=form.order.data,
                        subsection_id=form.subsection_id.data,
                        picture_url=form.picture_url.data,
                        external_url=form.external_url.data,
                        date=dt.now(),
                        markdown=form.markdown.data,
                        featured=form.featured.data
                        )

        data_reorder = eval(_type).query.all()
        for d in data_reorder:
            if d.order >= form.order.data:
                d.order += 1

        db.session.add(new_data)
        db.session.commit()
        flash("Successfully added!", "success")
        return redirect(url_for('manage'))


    return render_template('add_edit.html', mission="Add", _type=_type, form=form)



@app.route('/edit', methods=['POST', 'GET'])
@login_required
def edit():
    _id = request.args.get("id")
    _type = request.args.get("type")
    if not _id or not _type:
        flash("Data does not exist", "warning")
        return redirect(url_for('manage'))

    _type = _type.title()
    table_list = ["Sections", "Subsections", "Content"]
    if _type not in table_list:
        print("Invalid page access")
        flash("Invalid table, does not exist", "warning")
        return redirect(url_for('manage'))

    edit_data = eval(_type).query.get(_id)

    form_type = _type + "_Add_Form" if _type in table_list else "force_eval_fail"
    form = eval(form_type)(request.form)

    paneltype_choices = [("1","Simple"), ("2","Subpanel"), ("3", "Article")]
    if _type=="Sections":
        form.paneltype.choices = paneltype_choices
    if _type=="Subsections":
        form.section_id.choices = [(str(x.id)+"", x.levelname) for x in Sections.query.all()]
    if _type=="Content":
        form.subsection_id.choices = [(str(x.id)+"", x.section.levelname + " - " + x.levelname) for x in Subsections.query.all()]

    if request.method=="GET":
        if _type=="Sections":
            form.paneltype.process_data(edit_data.paneltype)
            #form.paneltype.default = str(edit_data.paneltype)+""
            form.levelname.data = edit_data.levelname
            form.order.data = edit_data.order

        if _type=="Subsections":
            form.section_id.process_data(edit_data.section_id)
            #form.section_id.default = str(edit_data.section_id)+""
            form.levelname.data = edit_data.levelname
            form.order.data = edit_data.order

        if _type=="Content":
            form.subsection_id.process_data(edit_data.subsection_id)
            #form.subsection_id.default = str(edit_data.subsection_id)+""
            form.levelname.data = edit_data.levelname
            form.order.data = edit_data.order
            form.picture_url.data = edit_data.picture_url
            form.external_url.data = edit_data.external_url
            form.markdown.data = edit_data.markdown
            form.featured.data = edit_data.featured

    if form.validate_on_submit():

        if _type=="Sections":
            edit_data.paneltype = paneltype_choices[int(form.paneltype.data)-1][1] 
            edit_data.levelname = form.levelname.data 
            edit_data.order = form.order.data  

        if _type=="Subsections":
            edit_data.section_id = form.section_id.data
            edit_data.levelname = form.levelname.data
            edit_data.order = form.order.data

        if _type=="Content":
            edit_data.subsection_id = form.subsection_id.data
            edit_data.levelname = form.levelname.data
            edit_data.order = form.order.data
            edit_data.picture_url = form.picture_url.data
            edit_data.external_url = form.external_url.data
            edit_data.markdown = form.markdown.data
            edit_data.featured = form.featured.data

        db.session.commit()
        flash("Successfully editted!", "success")
        return redirect(url_for('manage'))

    return render_template('add_edit.html', mission="Edit", _type=_type, form=form)






