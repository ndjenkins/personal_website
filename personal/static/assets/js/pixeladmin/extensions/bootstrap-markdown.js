(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['jquery', 'px-libs/markdown', 'px-libs/bootstrap-markdown'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('jquery'), require('px-libs/markdown'), require('px-libs/bootstrap-markdown'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.jquery, global.markdown, global.bootstrapMarkdown);
    global.bootstrapMarkdown = mod.exports;
  }
})(this, function (_jquery) {
  'use strict';

  var _jquery2 = _interopRequireDefault(_jquery);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  // Extensions / Bootstrap-markdown
  // --------------------------------------------------

  (function ($) {
    'use strict';

    if (!$.fn.markdown) {
      throw new Error('bootstrap-markdown.js required.');
    }

    function fullscreenResizeHandler($header) {
      var height = $header.outerHeight();
      var $preview = this.$editor.find('.md-preview');

      this.$textarea[0].style.top = height + 'px';

      if ($preview.length) {
        $preview[0].style.top = height + 'px';
      }
    }

    var markdownBuildButtons = $.fn.markdown.Constructor.prototype.__buildButtons;
    var markdownSetFullscreen = $.fn.markdown.Constructor.prototype.setFullscreen;
    var markdownShowPreview = $.fn.markdown.Constructor.prototype.showPreview;

    $.fn.markdown.Constructor.prototype.__buildButtons = function (name, alter) {
      var $container = markdownBuildButtons.call(this, name, alter);

      $container.find('.btn-default').removeClass('btn-default').addClass('btn-secondary');

      return $container;
    };

    $.fn.markdown.Constructor.prototype.setFullscreen = function (mode) {
      markdownSetFullscreen.call(this, mode);

      // Enter fullscreen mode
      if (mode) {
        var $header = this.$editor.find('.md-header');

        fullscreenResizeHandler.call(this, $header);
        $(window).on('resize.md-editor', $.proxy(fullscreenResizeHandler, this, $header));

        // Exit fullscreen mode
      } else {
        this.$textarea[0].style.top = 'auto';
        this.$editor.find('.md-preview').css('top', 'auto');
        $(window).off('resize.md-editor');
      }
    };

    $.fn.markdown.Constructor.prototype.showPreview = function () {
      markdownShowPreview.call(this);

      if (this.$editor.hasClass('md-fullscreen-mode')) {
        var $header = this.$editor.find('.md-header');

        fullscreenResizeHandler.call(this, $header);
      }
    };
  })(_jquery2.default);
});