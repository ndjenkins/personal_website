(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['jquery', 'px-libs/perfect-scrollbar.jquery'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('jquery'), require('px-libs/perfect-scrollbar.jquery'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.jquery, global.perfectScrollbar);
    global.perfectScrollbarJquery = mod.exports;
  }
})(this, function (_jquery) {
  'use strict';

  var _jquery2 = _interopRequireDefault(_jquery);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  // Extensions / Bootstrap-datepicker
  // --------------------------------------------------

  (function ($) {
    'use strict';

    if (!$.fn.perfectScrollbar) {
      throw new Error('perfect-scrollbar.jquery.js required.');
    }

    var _isRtl = $('html').attr('dir') === 'rtl';
    var fnPerfectScrollbar = $.fn.perfectScrollbar;

    $.fn.perfectScrollbar = function (settingOrCommand) {
      return this.each(function () {
        var _this = this;

        var psId = $(this).attr('data-ps-id');

        fnPerfectScrollbar.call($(this), settingOrCommand);

        if (_isRtl && !psId) {
          psId = $(this).attr('data-ps-id');

          if (psId) {
            $(window).on('resize.ps.' + psId, function () {
              return $(_this).perfectScrollbar('update');
            });
          }
        } else if (_isRtl && psId && settingOrCommand === 'destroy') {
          $(window).off('resize.ps.' + psId);
        }
      });
    };
  })(_jquery2.default);
});