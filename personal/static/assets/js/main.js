// ===============================================================================
// Main script
//

// Config
requirejs.config({
  baseUrl: '/static/assets/js'
});

// Initialize
require(['jquery', 'px/pixeladmin', 'px/plugins/px-nav', 'px/plugins/px-navbar', 
	'px/extensions/bootstrap-markdown', 'px/plugins/px-footer'], function($) {
  $('body > .px-nav').pxNav();
  $('body > .px-footer').pxFooter();
});
