# manage.py
import os
import pandas as pd

from flask_script import Manager

from personal import app
from personal.models import *

manager = Manager(app)

@manager.command
def drop_db():
	# Run this to drop the database schema
    db.drop_all()

@manager.command
def create_db():
	# Run this to create the database schema
    db.create_all()

@manager.command
def create_data():
	# Run this to fill the database schema according to the dummy data spreadsheet
    sheets = ["Sections", "Subsections", "Content", "Admin"]
    path_to_model = "database/data_model.xlsx"
    xl = pd.ExcelFile(path_to_model)
    for sheet in sheets:
        print(sheet)
        df = xl.parse(sheet)
        for ind, row in df.iterrows():
            test_data = eval(sheet)(**row.to_dict())
            db.session.add(test_data)
        db.session.commit()


if __name__ == '__main__':
    manager.run()
