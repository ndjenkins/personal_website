## README.md

Index:
*  What is this and why
*  For tinkering - Those that want to learn technically how this works
*  For use - Those that want to manage content on the site


# What is this?

This is the application and set of instructions to create your own personal blog site on heroku. This guide is intended to be for developers new to web programming, but familiarity with basic programing concepts, including some understanding of classes and methods. This guide is of particular benefit for data scientists, who know python through using pandas and machine learning libraries, and want to get a deeper understanding of backend development.

### Why?

I think it's important for data scientists to understand backend development. We need to not just create machine learning models but understand how to make them available in production. While this guide does not go into this learning goal, the aim is to introduce basic backend concepts including..

- Devops, managing python environments and managing a platform as a service (PaaS)
- Databases, learning better data storage and retrival methods using relational databases and CRM
- Backend, using python's flask library to build APIs and web routes

This guide does culminate in a frontend web site, specifically my own personal website, but frontend is not the specific learning goal either. Importantly to note; this guide uses PixelAdmin frontend code. The authors of this project have created some fantastic template webpages and introduction to using bootstrap. For a relatively minimal time investment, you can create frontend pages that look pretty and professional. This allows us to avoid going down an orthoganal learning path of frontend web development and design, when our focus is backend development.

With that important caveat in mind; please do pay for a license to use PixelAdmin. For $7 you seriously will not regret it, as there are many templates and options.


# For tinkering

### Installations
You will need python 3.6+, some libraries, a git installation, and the heroku command line tool. Python you should have otherwise shoo. Libaries, Git and Heroku CLI steps covered below.

Also recommend getting postgres 10 or 9.6 installed, with a database viewer such as dbeaver, or pgadmin4 is fine if it comes with it.

Make a local directory, load command prompt, navigate there and perform a git clone operation.
''' code
git clone https://evolveresearch@bitbucket.org/evolveresearch/personal_website.git
'''

This is on my bitbucket account. If you need git find a download for sourcetree.
https://devcenter.heroku.com/articles/getting-started-with-python#introduction

### Getting the database set up

The structure of the database for this project is pretty simple, and there will not be a lot of data recorded. As a result, the whole database file starts out quite small. Because of this, it's easier to seed the Heroku database with a copy of a basic database which I have set up previously.

For those that want to play with the project files locally, you will need a local postgres database. From here you can make updates and sync with Heroku. This is a several step process.

Firstly, you will need to install postgres, and set up a database name and password. Use the default username (postgres) and port (5432). Next you will want to create the schema and fill the database with basic data for the site to function. Set the environment variables below from slappy and happy.

```python
>>>set POSTGRES_PASSWORD=slappy
>>>set POSTGRES_DB=happy
>>>python manage.py drop_db
>>>python manage.py create_db
>>>python manage.py create_data
>>>python manage.py runserver
```

Any changes you make locally will not be reflected in the Heroku database. You will need to 1) export the local database to a file 2) tell Heroku to pick up that file, destroy current database and reinstanciate with your data. This is where the _'_save_database_to_heroku.bat'_ file comes in handy. You will need to change some details such as pointing to your postgres installation, postgres login information and your personal dropbox location.

Regarding dropbox, you will need to first create the database dump file, then make the link sharable. The link is what gets shared with Heroku to do the database restore.

The reverse, which is pulling down from Heroku is handled in the _'_load_database_locally.bat'_ file.

### Get a Heroku account, and follow the steps here;
https://devcenter.heroku.com/articles/getting-started-with-python#introduction


### Get the required python libraries

Heroku uses pipenv to manage and lockdown libraries, so that production matches local development. We will do the same locally by running these two commands. Alternatively, just use pip install <X> for each of the libraries listed in the pipfile.

```command line
pipenv --three
pipenv install
```

# For usage

### Getting you account set up

The site comes with an admin portal that allows you to customise the general look and feel and the front page. Access the portal by typing in /login to the end of the URL. First time users enter admin, secret.

Next you can access the portal by typing in /manage to the end of the URL, or clicking the small button at the bottom of the page, then selecting 'Manage Admin / Site'.

You can change the password on this page, be careful, password recovery is not possible!



### Creating content for the site

Content for this project is managed in a hierarchical structure.

*  Site
*  Sections
*  Subsections
*  Content

Site represents the top layer, there is one site. All other layers can have multiple instances. For example, you may want to have a 'Portfolio' Section, with a few Subsections (business, personal). Within each of those, you have some individual Content.

The navigation of the site, in particular the dropdown menus, correspond to some naming information as part of the Sections and Subsections.

There are three varieties of 'Section'. Choosing different varieties will make the page content display differently.
*  Simple - This shows all of the 'Content' in a single panel. Suitable for longform text or CV's.
*  Subpanel - This allows 'Content' to be represented by image based boxes with each 'Subsection'. Clicking the boxes will load a special page for the full content. Suitable for portfolio work or links.
*  Article - This has two views, 'Content' as the full article text, and 'Subsection' details as the blurb. Useful for blog posts or other  writing.

### Adding images to content

Heroku can only store content in the database as text. Because of this, images need to be stored seperately and linked to. For this I recommend using imgur.com. Upload an image, then navigate to the page. If you add '.png' to the end of the URL you will obtain a direct link to the image.

Next you can embed the image into your content by using some code called 'Markup language'. You can use this language to add other sorts of formats to your content including *boldness*, _italics_, tables, headings etc. This page is also written in Markup so feel free to use some as a template.



